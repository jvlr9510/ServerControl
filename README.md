# Arquitetura do Sistema

O diagrama a seguir representa a arquitetura do sistema de controle e comando (C&C) com um servidor e backdoors em máquinas Linux:

```
| Server Control | -----> | Internet | -----> | Alvos |
```

## Requisitos de Software

- Python 3.x
- Biblioteca `pyautogui` para controle de mouse e teclado. Você pode instalá-la usando o comando `pip install pyautogui`.
- Biblioteca `termcolor` para colorir a saída do terminal. Você pode instalá-la usando o comando `pip install termcolor`.

## Instruções de Uso

Para o funcionamento correto do sistema, é necessário seguir as seguintes instruções:

1. **Server.py**: Ao executar o `server.py`, certifique-se de informar o IP correto do servidor para estabelecer a conexão correta.
2. **Backdoor.py**: Ao enviar o `backdoor.py` para os alvos, certifique-se de corrigir o IP para o mesmo do servidor, garantindo assim a estabilidade da conexão.

## Referências

- [YouTube](https://www.youtube.com/@Cyph3rSec)
- [Medium - Programming a Malicious Backdoor and C2 Server in Python](https://medium.com/@whiteha0x07mania/programming-a-malicious-backdoor-and-c2-server-in-python-eda6e6a57257)
- [Secure Coding - How to Build a Simple Backdoor in Python](https://www.securecoding.com/blog/how-to-build-a-simple-backdoor-in-python/)
